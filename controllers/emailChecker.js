const request = require('request');
const con = require('../db/db.js');
exports.postEmailCheck = (req, res) =>{
	var data = {
		email: req.body.email
	}
 
	con.query('SELECT email FROM users where email = ?',[data.email], function(err, rows, field){
	if(err){ 
		res.json({code:400, emailExist: err});
	}
	if(rows.length){
		res.json({code:200, emailExist: true});
	
	}
	else
		{
			res.json({code:200, emailExist: false});
		}
			
	});
 
  
};


exports.postUsernameChecker = (req, res) =>{
	var data = {
		username: req.body.username
	}
 
	con.query('SELECT * FROM users where username = ?',[data.username], function(err, rows, field){
		if(err){ 
			res.json({code:400, UserName: err});
		}
	
		if(rows.length){
			res.json({code:200, UserName: true});
		
		}
		else
		{
			res.json({code:200, UserName: false});
		}

			
	});
 
  
};