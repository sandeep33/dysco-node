
var passport = require("../config/passport");
const request = require('request');
const requireLogin = require('../middlewares/requireLogin');
exports.postLogin = (err, result, next) => {
	passport.authenticate('local', {
            successRedirect : '/profile', // redirect to the secure profile section
            failureRedirect : '/login' // redirect back to the signup page if there is an error
           // failureFlash : true // allow flash messages
		}),
        function(req, res) {
            console.log("hello");

            if (req.body.remember) {
              req.session.cookie.maxAge = 1000 * 60 * 3;
            } else {
              req.session.cookie.expires = false;
            }
        res.redirect('/');
    };
}
exports.getLogin = (req, res) => {
	res.json("hello");
}