const { promisify } = require('util');
const request = require('request');
const express = require('express');
var app = express();

const passport = require('../config/passport');
app.post('/login', passport.authenticate('local', {
            successRedirect : 'success', // redirect to the secure profile section
            failureRedirect : '/fail' // redirect back to the signup page if there is an error
            //failureFlash : true // allow flash messages
		}),
        function(req, res) {
            console.log("hello");

            if (req.body.remember) {
              req.session.cookie.maxAge = 1000 * 60 * 3;
            } else {
              req.session.cookie.expires = false;
            }
        res.redirect('/');
    });
	
module.exports = app; 


