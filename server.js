const express = require('express');
const mysql = require('mysql');
var bodyParser = require('body-parser');
var bcrypt = require('bcrypt-nodejs');
const port = 3000;
var app = express();
const con = require('./db/db.js');
var cors = require('cors');


var passport = require("passport");

var session  = require('express-session');
var cookieParser = require('cookie-parser');

var login = require('./routes/routes.js');

var getEmailCheckController = require('./controllers/emailChecker');
var signupController = require('./controllers/registration');
var locationController = require('./controllers/location');
var designationController = require('./controllers/designation');
var testController = require('./controllers/test');
var getSkillsController = require('./controllers/getSkill');
var getFilterFollowListController = require('./controllers/getFilterFollowList');
var getFilterFollowListServicesController = require('./controllers/getFilterFollowListServices');
var followeuserController = require('./controllers/followuser');

var profileDataController = require('./controllers/profileData');
var postController = require('./controllers/post');

var api = require('./routes/api.js');
app.use(cors());
app.use(session({
	secret: 'asdfghjklmnbvcxzqwertyuiop0123456789',
	resave: true,
	saveUninitialized: true
 } )); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.get('/', api.getApi);
//sign up
app.post('/test', testController.postTest);
app.post('/signupfirst', signupController.postFirtsSignUp);
app.post('/signup', signupController.postSignUp);
app.post('/emailcheck', getEmailCheckController.postEmailCheck);
app.post('/usernamecheck', getEmailCheckController.postUsernameChecker);
//location and service name
app.post('/location', locationController.postLocation);
app.get('/locationName', locationController.getLocationName);
app.get('/serviceName', locationController.getServiceName);
//industry name
app.get('/industryName', locationController.getIndustryName);
//designation name
app.get('/designation', designationController.getDesignation);

app.post('/designation', designationController.postDesignation);
// skills
app.get('/getskills', getSkillsController.getskills);

//get filter follow data
app.get('/filterfollowskill', getFilterFollowListController.getFilterFollowList);
app.get('/filterfollowservices', getFilterFollowListServicesController.getFilterFollowListServices);

app.post('/followeuser', followeuserController.postFollowUser);
// login
//app.post('/login', loginController.postLogin);
//app.get('/login', loginController.getLog.in);
app.use(login);

//profile data
app.get('/profile', profileDataController.getProfileData);

//post send
app.get('/post', postController.getPost);
app.post('/post', postController.postPost);

app.listen(port,() =>{
	console.log('Server used port:' +port);
});